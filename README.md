![N|Solid](logo_ioasys.png)

# README #

Empresas é um aplicativo Android que faz o login de usuário na API iOasys e recupera uma lista de empresas. Selecionando uma empresa da lista, é mostrada sua descrição.

### Como usar ###

* Abra a aplicação e insira os dados de teste:
	* Usuário de Teste: testeapple@ioasys.com.br
	* Senha de Teste : 12341234
* Após o login, a aplicação retornará uma lista de empresas, o tipo de cada uma e seu país.
* Tocando no ícone de pesquisa é possível pesquisar pelo nome da empresa. A pesquisa retorna as empresas cujo nome contenha qualquer parte da chave de pesquisa.
* Selecionando uma empresa da lista, é exibido um texto de detalhamento da mesma, tal como sua imagem.
* Ao voltar para a tela de login, é necessário inserir a senha novamente.


### Bibliotecas usadas ###

* Retrofit2, para requisições na API - foi utilizada por ser simples e eficaz.
* Gson, para analisar o Json - também é simples e eficaz.
* OkHttp3 logging interceptor, para o log das requisições
* Glide, para download das imagens


### Se tivesse mais tempo... ###

* ...teria feito testes unitários;
* ...teria modificado a barra de pesquisa - ela não some quando terminamos a pesquisa;
* ...teria adicionado mais opções de pesquisa (país, tipo)



### Screenshots ###
![N|Solid](Screenshot_1.png)
![N|Solid](Screenshot_2.png)
![N|Solid](Screenshot_3.png)
![N|Solid](Screenshot_4.png)

### Contato ###

Camila Araújo  

camila_c_araujo@outlook.com  
(31) 988617126


