package com.example.kamilazoldyek.empresas_android.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kamilazoldyek.empresas_android.R;
import com.example.kamilazoldyek.empresas_android.api.ApiManager;
import com.example.kamilazoldyek.empresas_android.model.LoginResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.example.kamilazoldyek.empresas_android.constants.Constants.LOG_TAG;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private EditText user_email, user_senha;
    private Button button_login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        user_email = findViewById(R.id.edit_text_email);
        user_senha = findViewById(R.id.edit_text_senha);
        button_login = findViewById(R.id.button_login);

        button_login.setOnClickListener(this);
    }

    protected void onResume() {
        super.onResume();
        user_senha.getText().clear();
    }

    private void login() {

        String email = user_email.getText().toString().trim();
        String senha = user_senha.getText().toString().trim();


        if (email.isEmpty()) {
            Toast.makeText(this, "E-mail requerido", Toast.LENGTH_SHORT).show();
            user_email.requestFocus();
            return;
        }
        if (senha.isEmpty()) {
            Toast.makeText(this, "Senha requerida", Toast.LENGTH_SHORT).show();
            user_email.requestFocus();
            return;
        }

        Call<LoginResponse> call = ApiManager.getInstance().getApi().userLogin(email, senha);

        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                LoginResponse loginResponse = response.body();
                String responseMessage = response.message();

                if (responseMessage.equals("Unauthorized")) {
                    Toast.makeText(MainActivity.this, "Usuário ou senha incorretos", Toast.LENGTH_LONG).show();
                    user_email.requestFocus();
                } else if (responseMessage.equals("OK")) {
                    if (!loginResponse.isSuccess()) {
                        Toast.makeText(MainActivity.this, responseMessage, Toast.LENGTH_LONG).show();
                    } else {
                        String access_token = response.headers().get("access-token");
                        String client = response.headers().get("client");
                        String uid = response.headers().get("uid");

                        Intent intent = new Intent(MainActivity.this, EnterpriseListActivity.class);
                        intent.putExtra("ACC", access_token);
                        intent.putExtra("CLIENT", client);
                        intent.putExtra("UID", uid);
                        startActivity(intent);

                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.d(LOG_TAG, "OnFailure: " + t.getMessage());
                Toast.makeText(MainActivity.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }


    @Override
    public void onClick(View v) {
        login();
    }


}
