package com.example.kamilazoldyek.empresas_android.api;

import com.example.kamilazoldyek.empresas_android.model.EnterpriseList;
import com.example.kamilazoldyek.empresas_android.model.LoginResponse;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface Api {

    @FormUrlEncoded
    @POST("users/auth/sign_in")
    Call<LoginResponse> userLogin(
            @Field("email") String email,
            @Field("password") String password
    );

    @GET("enterprises")
    Call<EnterpriseList> getEnterprises(
            @Header("access-token") String access_token,
            @Header("client") String client,
            @Header("uid") String uid
    );


}
