package com.example.kamilazoldyek.empresas_android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EnterpriseType {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("enterprise_type_name")
    @Expose
    private String enterpriseTypeName;

    public Integer getId() {
        return id;
    }

    public String getEnterpriseTypeName() {
        return enterpriseTypeName;
    }
}
