package com.example.kamilazoldyek.empresas_android.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.io.Serializable;
import java.util.List;

public class EnterpriseList implements Serializable {
    @SerializedName("enterprises")
    @Expose
    private List<Enterprise> enterprises = null;

    public List<Enterprise> getEnterprises() {
        return enterprises;
    }
}
