package com.example.kamilazoldyek.empresas_android.api;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static com.example.kamilazoldyek.empresas_android.constants.Constants.BASE_URL_API;

public class ApiManager {

    private static ApiManager mInstance;
    private Retrofit retrofit;


    private ApiManager() {
        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL_API)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }


    public static synchronized ApiManager getInstance() {
        if (mInstance == null) {
            mInstance = new ApiManager();
        }
        return mInstance;
    }

    public Api getApi() {
        return retrofit.create(Api.class);
    }
}
