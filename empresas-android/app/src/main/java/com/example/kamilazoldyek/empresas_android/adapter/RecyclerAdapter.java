package com.example.kamilazoldyek.empresas_android.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.kamilazoldyek.empresas_android.R;
import com.example.kamilazoldyek.empresas_android.activity.DetailActivity;
import com.example.kamilazoldyek.empresas_android.model.Enterprise;

import java.util.ArrayList;
import java.util.List;

import static com.example.kamilazoldyek.empresas_android.constants.Constants.BASE_URL;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.RecyclerAdapterViewHolder> implements Filterable {


    private List<Enterprise> enterpriseList;
    private List<Enterprise> enterpriseListCOPY;
    String enterpriseName, enterpriseImage, enterpriseDescription;
    Context mContext;


    public RecyclerAdapter(List<Enterprise> enterpriseList, Context mContext) {
        this.enterpriseList = enterpriseList;
        this.mContext = mContext;
        enterpriseListCOPY = new ArrayList<>(enterpriseList);
    }

    @NonNull
    @Override
    public RecyclerAdapter.RecyclerAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v;
        v = LayoutInflater.from(mContext).inflate(R.layout.card_item, parent, false);
        final RecyclerAdapter.RecyclerAdapterViewHolder vHolder = new RecyclerAdapterViewHolder(v);

        return vHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerAdapter.RecyclerAdapterViewHolder holder, int position) {

        final Enterprise enterprise = enterpriseList.get(position);

        enterpriseName = String.valueOf(enterprise.getEnterpriseName());
        enterpriseDescription = String.valueOf(enterprise.getDescription());
        enterpriseImage = String.valueOf(enterprise.getPhoto());


        holder.extras.putString("ENTERPRISE_NAME", enterpriseName);
        holder.extras.putString("ENTERPRISE_IMAGE", enterpriseImage);
        holder.extras.putString("ENTERPRISE_DESCRIPTION", enterpriseDescription);

        holder.enterpriseNameTV.setText(enterpriseName);
        holder.enterpriseTypeTV.setText(String.valueOf(enterprise.getEnterpriseType().getEnterpriseTypeName()));
        holder.enterpriseCountryTV.setText(String.valueOf(enterprise.getCountry()));

        String imageURL = String.valueOf(enterprise.getPhoto());

        Glide
                .with(mContext)
                .load(BASE_URL + imageURL)
                .error(R.drawable.ic_image)
                .into(holder.enterpriseImageView);

    }

    @Override
    public int getItemCount() {
        return enterpriseList.size();
    }

    @Override
    public Filter getFilter() {
        return enterpriseFilter;

    }

    private Filter enterpriseFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            List<Enterprise> filteredList = new ArrayList<>();
            if (constraint == null || constraint.length() == 0) {
                filteredList.addAll(enterpriseListCOPY);
            } else {
                String filterPattern = constraint.toString().toLowerCase().trim();
                for (Enterprise enterprise : enterpriseListCOPY) {
                    if (enterprise.getEnterpriseName().toLowerCase().contains(filterPattern)) {
                        filteredList.add(enterprise);
                    }
                }

            }
            FilterResults results = new FilterResults();
            results.values = filteredList;
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            enterpriseList.clear();
            enterpriseList.addAll((List) results.values);
            notifyDataSetChanged();

        }
    };


    public class RecyclerAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public LinearLayout card_item;
        public TextView enterpriseNameTV;
        public TextView enterpriseTypeTV;
        public TextView enterpriseCountryTV;
        public ImageView enterpriseImageView;
        public Bundle extras;


        public RecyclerAdapterViewHolder(View itemView) {

            super(itemView);
            extras = new Bundle();
            card_item = itemView.findViewById(R.id.card_item);
            enterpriseNameTV = itemView.findViewById(R.id.enterpriseName);
            enterpriseTypeTV = itemView.findViewById(R.id.enterpriseType);
            enterpriseCountryTV = itemView.findViewById(R.id.enterpriseCountry);
            enterpriseImageView = itemView.findViewById(R.id.enterpriseImage);

            card_item.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {

            Intent intent = new Intent(mContext, DetailActivity.class);
            intent.putExtras(extras);
            mContext.startActivity(intent);


        }
    }
}
