package com.example.kamilazoldyek.empresas_android.constants;

public final class Constants {

    public static final String BASE_URL_API = "http://empresas.ioasys.com.br/api/v1/";
    public static final String BASE_URL = "http://empresas.ioasys.com.br/";
    public static final String LOG_TAG = "Teste";
}
