package com.example.kamilazoldyek.empresas_android.activity;

import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import com.example.kamilazoldyek.empresas_android.R;
import com.example.kamilazoldyek.empresas_android.adapter.RecyclerAdapter;
import com.example.kamilazoldyek.empresas_android.api.ApiManager;
import com.example.kamilazoldyek.empresas_android.model.Enterprise;
import com.example.kamilazoldyek.empresas_android.model.EnterpriseList;
import java.util.ArrayList;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import static com.example.kamilazoldyek.empresas_android.constants.Constants.LOG_TAG;

public class EnterpriseListActivity extends AppCompatActivity {


    public RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;
    private List<Enterprise> enterpriseList;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.enterprise_list);

        String access_token = getIntent().getStringExtra("ACC");
        String client = getIntent().getStringExtra("CLIENT");
        String uid = getIntent().getStringExtra("UID");

        mRecyclerView = findViewById(R.id.recycler_view);
        enterpriseList = new ArrayList<>();

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        getEnterprises(access_token, client, uid);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    public void setupRecyclerView(List<Enterprise> list){

        LinearLayoutManager layoutManager =new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
        mAdapter = new RecyclerAdapter(list, EnterpriseListActivity.this);
        mRecyclerView.setAdapter(mAdapter);

        DividerItemDecoration itemDecor =new DividerItemDecoration(EnterpriseListActivity.this, DividerItemDecoration.VERTICAL);
        itemDecor.setDrawable(ContextCompat.getDrawable(EnterpriseListActivity.this,R.drawable.rectangle));
        mRecyclerView.addItemDecoration(itemDecor);
    }

    public void getEnterprises(String access_token, String client, String uid) {

        Call<EnterpriseList> call = ApiManager.getInstance().getApi().getEnterprises(access_token, client, uid);

        call.enqueue(new Callback<EnterpriseList>() {
            @Override
            public void onResponse(Call<EnterpriseList> call, Response<EnterpriseList> response) {
                if(!response.isSuccessful()){
                    Log.d(LOG_TAG, response.message());
                    return;
                }else{

                    if (response.body() == null){
                        Log.d(LOG_TAG, "empty response.body");
                    }else {

                        enterpriseList = response.body().getEnterprises();
                        setupRecyclerView(enterpriseList);
                    }
                }
            }

            @Override
            public void onFailure(Call<EnterpriseList> call, Throwable t) {
                Log.d(LOG_TAG, t.getMessage());
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.search_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setImeOptions(EditorInfo.IME_ACTION_DONE);
//        searchView.setQueryHint("Pesquisar");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return false;
            }
        });
        return true;
    }
}
