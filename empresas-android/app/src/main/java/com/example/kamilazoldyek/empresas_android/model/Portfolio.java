package com.example.kamilazoldyek.empresas_android.model;

public class Portfolio {
    private int enterprises_number;
    private String[] enterprises;

    public Portfolio(int enterprises_number, String[] enterprises) {
        this.enterprises_number = enterprises_number;
        this.enterprises = enterprises;
    }

    public int getEnterprises_number() {
        return enterprises_number;
    }

    public String[] getEnterprises() {
        return enterprises;
    }
}
