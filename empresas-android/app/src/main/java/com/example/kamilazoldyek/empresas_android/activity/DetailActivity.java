package com.example.kamilazoldyek.empresas_android.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.kamilazoldyek.empresas_android.R;

import static com.example.kamilazoldyek.empresas_android.constants.Constants.BASE_URL;

public class DetailActivity extends AppCompatActivity {

    public TextView enterpriseDescriptionTV;
    public ImageView enterpriseImageView;
    public String actionBarTitle;
    public String enterpriseDescription;
    public String imageURL;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.enterprise_description);

        Bundle extras = getIntent().getExtras();

        actionBarTitle = extras.getString("ENTERPRISE_NAME");
        enterpriseDescription = extras.getString("ENTERPRISE_DESCRIPTION");
        imageURL = extras.getString("ENTERPRISE_IMAGE");

        enterpriseDescriptionTV = findViewById(R.id.description_text);
        enterpriseImageView = findViewById(R.id.description_image);


        Glide
                .with(this)
                .load(BASE_URL + imageURL)
                .error(R.drawable.ic_image)
                .into(enterpriseImageView);

        enterpriseDescriptionTV.setText(enterpriseDescription);

        getSupportActionBar().setTitle(actionBarTitle);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
