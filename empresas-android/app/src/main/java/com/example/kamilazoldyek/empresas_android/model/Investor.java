package com.example.kamilazoldyek.empresas_android.model;

public class Investor {

    private int id;
    private String investor_name;
    private String email;
    private String city;
    private String country;
    private int balance;
    private String photo;
    private String password;
    private Portfolio portfolio;
    private int portfolio_value;
    private boolean first_access;
    private boolean super_angel;


}
